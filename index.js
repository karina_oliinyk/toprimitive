Object.prototype[Symbol.toPrimitive] = function (hint) {
  console.log(`Convert in ${hint}`);

  let result;

  if (hint == 'string') {
    result = this.toString();
    if (typeof(result) !== 'object') {
      return result;
    } else if(typeof(this.valueOf()) !== 'object') {
      return this.valueOf();
    }
    throw new TypeError('Can not convert object to primitive')
  }

  else {
    result = this.valueOf();
    if (typeof(result) !== 'object') {
      return result;
    } else if(typeof(this.toString()) !== 'object') {
      return this.toString();
    }
    throw new TypeError('Can not convert object to primitive')
  }
}

const a = { name: "Frenk" };
const b = { name: "Alice"}
// string
console.log(String(a));
console.log(`Its an object ${a}`);
console.log(alert(a));
// number
console.log(a / 2);
console.log(+a);
console.log(a > b);
// default
console.log(a + b);
console.log(a + []);
console.log([] + []);
console.log({} + ''});

